<?php
header('Content-Type: text/html; charset=utf-8');
require (DIRNAME(__FILE__) . '/curl.php');
require (DIRNAME(__FILE__) . '/simple_html_dom.php');

class Api {

    protected $displayErrors = false;

    protected $from = null;

    protected $to = null;

    protected $time = null;

    protected $base = "http://www.m.rozkladzik.pl/krakow/";

    protected $limit = 5;

    public function __construct() {
        if (isset($_GET['from'])) {
            $this->from = $_GET['from'];
        }
        if (isset($_GET['to'])) {
            $this->to = $_GET['to'];
        }
        if (isset($_GET['limit']) && is_numeric($_GET['limit'])) {
            $limit = (int) $_GET['limit'];
            if ($limit > 0) {
                $this->limit = $limit;
            }
        }
        if (isset($_GET['time'])) {
            $this->time = $_GET['time'];
        } else {
            $this->time = date('H:i');
        }
        
        $this->setErrorDisplay();
    }

    public function setErrorDisplay() {
        if ($this->displayErrors) {
            ini_set('display_errors', 1);
            error_reporting(E_ALL | E_STRICT);
        } else {
            ini_set('display_errors', 0);
            error_reporting(0);
        }
    }

    public function get() {
        if (is_null($this->from)) {
            return $this->error('Podaj przystanek początkowy.');
        }
        if (is_null($this->to)) {
            return $this->error('Podaj przystanek końcowy.');
        }
        if ($this->from == $this->to) {
            return $this->error('Nazwa przystanku początkowego i końcowego nie może być taka sama.');
        }
        $splitDate = explode(':', $this->time, 2);
        if ($splitDate[0] < 0 || $splitDate[0] > 23 || $splitDate[1] < 0 || $splitDate[1] > 59) {
            return $this->error('Podana data jest nieprawidłowa.');
        }
        
        return $this->process();
    }

    private function getString($str) {
        if (is_null($str)) {
            return null;
        }
        
        $x = new mycurl("{$this->base}wyszukiwarka_polaczen.html?fromSearch=" . urlencode($str));
        $x->createCurl();
        preg_match_all('~<a href=\'(.*)\'(.*)>(.*)</a>~iU', $x->__tostring(), $matches);
        
        $index = 0;
        foreach ($matches[3] as $key => $match) {
            if ($str == trim($match)) {
                $index = $key;
            }
        }
        
        if (! empty($matches[1][$index])) {
            parse_str(parse_url($matches[1][$index], PHP_URL_QUERY), $c);
            if (! empty($c['from'])) {
                return $c['from'];
            }
        }
        
        return null;
    }

    public function process() {
        mysql_connect('localhost', "elegant_mpk", 'elegant_mpk_admin') or die($this->error('Database connection error.'));
        mysql_select_db('elegant_mpk') or die($this->error('Database connection error.'));
        mysql_query("SET NAMES utf8");
        
        // linia od:
        $result = mysql_query("SELECT * FROM przystanki WHERE nazwa LIKE '" . $this->from . "';");
        $row = mysql_fetch_array($result, MYSQL_ASSOC);
        $from = $row['nazwa'];
        $przystanek1Id = $row['id'];
        // linia do:
        $result2 = mysql_query("SELECT * FROM przystanki WHERE nazwa LIKE '" . $this->to . "';");
        $row2 = mysql_fetch_array($result2, MYSQL_ASSOC);
        $to = $row2['nazwa'];
        $przystanek2Id = $row2['id'];
        
        $from = $this->getString($from);
        $to = $this->getString($to);
        $hour = $this->time;
        
        if (is_null($from) || is_null($to)) {
            return $this->error('Nie znaleziono połączenia. Spróbuj ponownie wpisać poprawne dane.');
        }
        
        $from = urlencode($from);
        $to = urlencode($to);
        
        $url = "{$this->base}wyszukiwarka_polaczen.html?time={$hour}&day=1&profile=opt&minChangeTime=1&maxWalkChange=100&useOrIgnoreLines=use&ignoreUseLines=&from={$from}&to={$to}&js=1";
        
        $c = new mycurl($url);
        $c->createCurl();
        
        $_dom = str_get_html($c->__tostring());
        $partialsDiv = $_dom->find('.route_row_colapse');
        
        $parts = array();
        foreach ($partialsDiv as $div) {
            foreach ($div->find('.show_rest') as $rest) {
                $rest->innertext = '';
            }
            $parts[] = '<div>' . $div->innertext . '</div>';
        }
        
        $limit = $this->limit;
        
        $realData = array();
        foreach ($parts as $key => $part) {
            if ($key >= $limit) {
                break;
            }
            $dom = str_get_html($part);
            $firstTr = $dom->find('.route_sum_row', 0)->find('tr', 0);
            
            $lines = array();
            foreach ($firstTr->find('.line_name') as $line) {
                $lines[] = trim($line->plaintext);
            }
            $time = $firstTr->find('.time', 0)->plaintext;
            
            preg_match('~<\/span><br>(.*)</td>~iU', $firstTr->innertext, $match);
            
            $_data = array(
                'time' => trim($time),
                'hours' => trim($match[1]),
                'lines' => $lines,
                'stops' => array()
            );
            
            $stopsTrs = $dom->find('.route_desc', 0)->find('tr');
            foreach ($stopsTrs as $tr) {
                $_name = $tr->find('.name', 0);
                if ($_name) {
                    $_b = $_name->find('b', 0);
                    $_time = $tr->find('.time', 0)->plaintext;
                    if ($_b && $_b->plaintext != $_name->plaintext) {
                        $split = explode('</b>', $_name->innertext);
                        $_data['stops'][] = array(
                            'type' => 'direction',
                            'line' => trim($_b->plaintext),
                            'name' => trim($split[1])
                        );
                    } else {
                        if ($_time) {
                            $_data['stops'][] = array(
                                'type' => 'stop',
                                'name' => trim($_name->plaintext),
                                'time' => trim($_time)
                            );
                        } else {
                            $_data['stops'][] = array(
                                'type' => 'walk',
                                'name' => trim($_name->plaintext)
                            );
                        }
                    }
                }
            }
            
            $realData[] = $_data;
        }
        
        echo json_encode(array(
            'status' => 1,
            'data' => $realData
        ));
    }

    public function error($msg) {
        $array = array(
            'status' => 0,
            'error' => $msg
        );
        
        echo json_encode($array);
    }
}

$api = new Api();
$api->get();