jQuery(document).ready(function($) {

	$('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({

		disableOn: 700,

		type: 'iframe',

		mainClass: 'mfp-fade',

		removalDelay: 160,

		preloader: false,

		showCloseBtn : true, 

		closeBtnInside: true,

		midClick: true, 
		
		closeMarkup:  '<button id="pop-close-click" title="%title%" class="mfp-close">Zmień linie</button>',



		fixedContentPos: false

	});

});



