<?php
if(isset($_POST['submit'])) {
    $name = $_POST['name'];
	$email = $_POST['email'];
	$message = $_POST['message'];
	$email_from = $name.'<'.$email.'>';

	if(($name=="")||($email=="")||($message=="")) {
		$error = 'Uzupełnij wszystkie pola!';
	}

	$to="piotrekha15@gmail.com"; //piotrekha15
	$subject="Formularz kontaktowy";
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
	$headers .= "From: ".$email_from."\r\n";

	$message="Name: {$name}<br />Email: {$email}<br />Wiadomość: {$message}";

	if(mail($to,$subject,$message,$headers)) {
		$success = true;
	}
	else {
		$error = 'Błąd przy wysyłaniu wiadomości!';
	}
 }

?>

<!doctype html>

<?php

    $db = mysqli_connect('hostname','username','password','databasename');

	mysql_query("SET NAMES 'utf8'");

	

?>



<html lang="pl">

	<head>

		<meta charset="UTF-8">

		<meta name="author" content="Tomaszewski, Sokulski, Szydlak, Piskór">

		<title>Informator MPK Kraków</title>

		<!---SCRIPTS--->

		<script type="text/javascript" src="scripts/html5.js"></script>

		<script type="text/javascript" src="scripts/jquery.js"></script>

		<script type="text/javascript" src="scripts/main.js"></script>

		<script type="text/javascript" src="scripts/placeholders.jquery.min.js"></script>

		<script type="text/javascript" src="scripts/magnific-popup/dist/jquery.magnific-popup.js"></script>

		<script type="text/javascript" src="scripts/popup.js"></script>

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

		<script type="text/javascript" src="scripts/validation.js"></script>

		<!---STYLESHEETS--->

		<link rel="stylesheet" type="text/css" href="css/style.css" media="all">

		<link rel="stylesheet" type="text/css" href="scripts/magnific-popup/dist/magnific-popup.css" media="all">

		<link rel="stylesheet" type="text/css" href="css/reset.css" media="all">

	</head>

	<body>

		<section class="billboard">



			<header>



				<div class="wrapper">



					<a href="index.php"><img src="img/logo.png" class="h_logo" alt="" title=""></a>



					<nav>



						<ul class="main_nav">



							<li><a href="index.php">Home</a></li>



							<li><a href="rozklad.php">Sprawdź rozkład</a></li>



							<li><a href="wyszukaj.php">Wyszukiwarka połączeń</a></li>



							<li class="current"><a href="kontakt.php">Kontakt</a></li>



						</ul>



					</nav>



				</div>



			</header>







			<section class="caption">



				<div class ="rozklad">

				

					<div class ="page-title">



						<h2>Kontakt</h2>



					</div>

				

					<div id="formularz">

						
						<?php if( !isset($success)):?>
						<form name="enq" method="post" action="kontakt.php" onsubmit="return validation();">

								<fieldset>

								    

								<input value="<?php echo isset($_POST['name']) ? $_POST['name'] : '';?>" type="text" name="name" id="name" value=""  class="input-block-level" placeholder="Imię i nazwisko" />

								<input value="<?php echo isset($_POST['email']) ? $_POST['email'] : '';?>"  type="text" name="email" id="email" value="" class="input-block-level" placeholder="Email" />

								<textarea rows="11" name="message" id="message" class="input-block-level" placeholder="Treść wiadomości"><?php echo isset($_POST['message']) ? $_POST['message'] : '';?></textarea>

								   

								<div class="sumbit">

									<input type="submit" value="Wyślij" name="submit" id="button-blue" class="btn btn-info pull-right" title="Kliknij, aby wysłać wiadomość!" />

									<div class = "ease">

									</div>

								</div>
								
								<?php if( $error ):?>
								<p class="alert"><?php echo $error;?></p>
								<?php endif;?>

								</fieldset>

						</form>
						<?php else:?>
						Wiadomość została pomyślnie wysłana
						<?php endif;?>

					

					</div>

					

				</div>



			</section>



		</section><!-- Billboard End -->







		<footer>



			<hr class="footer_sp"/>



			<p class="rights">Copyright© 2015 Tomaszewski, Sokulski, Szydlak, Piskór</p>



		</footer>



	</body>

</html>